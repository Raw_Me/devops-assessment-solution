# devops-assessment-solution

This is a solution for the devops-assessment by Hatim Abdullah.

https://github.com/HatimAbdullah/devops-assessment

- [Part I](#org18d3fdd)
- [Part II](#orgffad55b)
- [Part III](#orgeb58111)



<a id="org18d3fdd"></a>

# Part I

1.  First create ec2 instance (using ubuntu). A terraform yaml can be used for that.
    
    ```tf
    provider "aws" {
      profile = "default"
    }
    
    resource "aws_instance" "devops_assessment" {
      ami = "ami-00399ec92321828f5"
      instance_type = "t2.large"
      // Add your ssh keyname here
      key_name = ""
      // Add your security groups 
      security_groups = [
        "",
      ]
      tags = {
        Name = "devops-assessment"
      }
    
      connection {
        type     = "ssh"
        user     = "centos"
        // Add the private key file here
        private_key = file("")
        host        = self.public_ip
      }
    
      provisioner "remote-exec" {
        inline = [
          "sudo su",
          "cd ~"
          "git clone https://github.com/HatimAbdullah/devops-assessment.git",
        ]
      }
    }
    
    ```
2.  Clone the DevOps assessment project
    
    ```bash
    git clone https://github.com/HatimAbdullah/devops-assessment.git
    ```
3.  Install Docker Following the Official documentation (Note I prefer using the distro package manager over other installation methods and this will be used for all upcoming steps):
    
    ```bash
    sudo apt-get update
    
    sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
    
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    
    echo \
    "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    
    sudo apt-get update
    
    sudo apt-get install -y docker-ce docker-ce-cli containerd.io
    ```
4.  Install k3d
    
    ```bash
    wget -q -O - https://raw.githubusercontent.com/rancher/k3d/main/install.sh | bash
    ```


<a id="orgffad55b"></a>

# Part II

1.  Install make
    
    ```bash
    sudo apt install -y make
    ```
2.  Create k3d cluster
    
    ```bash
    cd devops-assessment
    
    make cluster
    ```
3.  Install kubectl
    
    ```bash
    snap install kubectl --classic
    ```
4.  Install helm
    
    ```bash
    sudo snap install helm --classic
    ```
5.  Install nginx ingress
    
    ```bash
    helm repo add nginx-stable https://helm.nginx.com/stable
    
    helm repo update
    
    helm install my-release nginx-stable/nginx-ingress
    ```
6.  Install promethues
    
    ```bash
    helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
    
    helm repo add stable https://charts.helm.sh/stable
    
    helm repo update
    
    helm install prometheus prometheus-community/kube-prometheus-stack
    ```


<a id="orgeb58111"></a>

# Part III

1.  build Dockerfile
    
    ```bash
         cd sample-service
    # note I modified the Dockerfile and tagged it with lib, by adding the following:
    ## RUN apt-get install libcap2-bin
    ## RUN setcap cap_net_bind_service=+ep `readlink -f \`which node\``
         docker build -t rawme/node-sample:latest .
         docker push rawme/node-sample:latest
    ```
2.  Create Deployment,svc,ingress Yaml for sample-service
    
    ```yaml
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: node-sample
      labels:
        app: node-sample
    spec:
      replicas: 1
      selector:
        matchLabels:
          app: node-sample
      template:
        metadata:
          labels:
            app: node-sample
        spec:
          securityContext: 
            fsGroup: 1001
            runAsUser: 1001
          containers:
          - name: node-sample
            image: rawme/node-sample:lib
            securityContext: 
              privileged: false
              capabilities:
                add:
                - CAP_NET_BIND_SERVICE
            resources:
              requests:
                memory: "64Mi"
                cpu: "250m"
              limits:
                memory: "512Mi"
                cpu: "500m"
            ports:
            - containerPort: 80
            volumeMounts:
            - mountPath: /var/local/aaa
              name: mydir
              readOnly: true
            - mountPath: /var/local/aaa/1.txt
              name: myfile
              readOnly: true
            livenessProbe:
              httpGet:
                path: /health
                port: 80
              initialDelaySeconds: 10
              periodSeconds: 5
            readinessProbe:
              httpGet:
                path: /info
                port: 80
              initialDelaySeconds: 10
              periodSeconds: 5
          volumes:
          - name: mydir
            hostPath:
              # Ensure the file directory is created.
              path: /var/local/aaa
              type: DirectoryOrCreate
          - name: myfile
            hostPath:
              path: /var/local/aaa/1.txt
              type: FileOrCreate
    ---
    apiVersion: v1
    kind: Service
    metadata:
      name: node-sample-svc
    spec:
      selector:
        app: node-sample
      ports:
        - protocol: TCP
          port: 80
          targetPort: 80
    ---
    apiVersion: extensions/v1beta1
    kind: Ingress
    metadata:
      name: node-sample-ingress
      annotations:
        ingress.kubernetes.io/rewrite-target: /
        kubernetes.io/ingress.class: nginx
    spec:
      rules:
      - host: ec2-18-216-237-210.us-east-2.compute.amazonaws.com
        http:
          paths:
          - path: /
            backend:
              serviceName: node-sample-svc
              servicePort: 80
    ```
